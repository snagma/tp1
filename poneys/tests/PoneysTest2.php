<?php
  require_once 'src/Poneys.php';

  class PoneysTest2 extends \PHPUnit_Framework_TestCase {

    /**
     * @dataProvider removeProvider
     */
    public function test_removePoneyFromField($x) {
      // Setup
      $Poneys = new Poneys();

      // Action
      $Poneys->removePoneyFromField($x);
      
      // Assert
      $this->assertTrue( $Poneys->getCount()>=0);
    }

    public function  removeProvider(){

      return [[5],[2],[9],[6]];
}
    
   public  function testName(){
    $mock = $this->getMockBuilder('class_name')
    ->disableOriginalConstructor()
    ->getMock();
    $mock->expects($this->at(1))->method('getNames')
         ->willReturn(['','']);

    $this->assertEquals(['',''],$mock->getNames());



}
  }
 ?>
